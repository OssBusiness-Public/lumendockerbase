#!/bin/bash

source docker/bash_functions.sh
importStackEnv

NEW_VERSION=$1

printHeaderBlock "Update docker enviroment version"

GIT_STATUS=$(git status -s)
if [ -n "$GIT_STATUS" ]; then
	printNoticeBlock "Commits pending, can not update version"
	git status -s
	exit 1
fi


# Fix stored stack info
sed -i "" "s/STACK_VERSION=.*/STACK_VERSION=$NEW_VERSION/" docker/.stack_env
sed -i "" "s/STACK_VERSION=.*/STACK_VERSION=$NEW_VERSION/" environment/.stack_env

# Fix docker image tags
OLD_VERSION_REGEXP=$STACK_NAME
OLD_VERSION_REGEXP+="\(_[a-z0-9_]*:\)$STACK_VERSION"
NEW_VERSION_STRING="$STACK_NAME\1$NEW_VERSION"
sed -i "" "s/$OLD_VERSION_REGEXP/$NEW_VERSION_STRING/g" docker-compose.yml
sed -i "" "s/$OLD_VERSION_REGEXP/$NEW_VERSION_STRING/g" docker-compose-dev.yml
sed -i "" "s/$OLD_VERSION_REGEXP/$NEW_VERSION_STRING/g" docker-compose-local.yml


git status -s

git add -A
git commit -m "Update version $STACK_VERSION => $NEW_VERSION"
git tag $NEW_VERSION

echo ""
echo ""
echo "Old version was: $STACK_VERSION"
echo "New version is: $NEW_VERSION"
printNoticeBlock "All images will be tagged with new version"
echo ""

exit 0