#!/usr/bin/env bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "List services for stack $STACK_NAME"

docker stack services lumen_docker_base