#!/bin/bash

source docker/bash_functions.sh

if ./docker_version.sh $1; then
    ./docker_build.sh
    ./docker_push.sh
else
    printNoticeBlock "Build and push was skipped"
fi
