## install new app


1. Download installer script
`docker  run  -v $(pwd):$(pwd) -w $(pwd) -i -t  bash /usr/local/bin/bash -c "apk update && apk add ca-certificates openssl && update-ca-certificates; wget https://gitlab.com/ossbusiness-public/lumendockerbase/raw/master/newApp.sh; chmod u+x newApp.sh;"`

2. Create new app
`./newApp.sh <owner> <app_name>`

