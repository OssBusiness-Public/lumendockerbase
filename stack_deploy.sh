#!/usr/bin/env bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "Deploying stack $STACK_NAME"

CMD="docker stack deploy --compose-file docker-compose.yml"
if [ -e docker-compose-local.yml ]; then
    echo "*** extend deploy with docker-compose-local.yml ***"
    CMD="$CMD --compose-file docker-compose-local.yml"
fi;

CMD="$CMD lumen_docker_base"
$CMD

echo ""
echo "Stack is booting up!"
echo "Stack is located at $STACK_PROTOCOL$STACK_HOST"
echo ""
echo ""
sleep 2