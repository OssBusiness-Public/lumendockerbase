#!/bin/bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "Push containers for stack $STACK_NAME"

docker-compose -f docker-compose.yml -f docker-compose-dev.yml push $1
