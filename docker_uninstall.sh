#!/bin/bash
docker stack rm lumen_docker_base
docker rmi $(docker images -a --filter "label=fi.oss-solutions.project=lumen_docker_base" -a -q)
