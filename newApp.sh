#!/bin/bash

echo ""
echo "#############################################"
echo "# Create new docker stack based application #"
echo "#############################################"
echo ""

PROJECT_OWNER=$1
PROJECT_NAME=$2
if [[ $PROJECT_NAME != *[!\ ]* ]]; then
	echo "Project Name is missing!" 
    exit 1
fi

rm -rf lumendockerbase $PROJECT_NAME

git clone https://gitlab.com/ossbusiness-public/lumendockerbase.git
cd lumendockerbase
git checkout-index --prefix=git-clone/ -a

grep --exclude=vendor -rl lumen_docker_base git-clone | xargs sed -i  "" "s/lumen_docker_base/$PROJECT_NAME/g"
grep --exclude=vendor -rl ossbusiness git-clone | xargs sed -i  "" "s/ossbusiness/$PROJECT_OWNER/g"

cd ..
mv lumendockerbase/git-clone $PROJECT_NAME
rm -rf lumendockerbase

cd $PROJECT_NAME
./docker_generate_enviroment_config.sh 

cd app
docker run -v $(pwd):$(pwd) -w $(pwd) -i -t composer install

cd ..
git init
git add -A
git commit -am "initial commit"

./docker_build.sh
./stack_deploy.sh


