#!/bin/bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "Build containers for stack $STACK_NAME"

rm -rf git-deploy-prod
git checkout-index --prefix=git-deploy-prod/ -a
docker-compose -f docker-compose.yml -f docker-compose-dev.yml build --no-cache $1
rm -rf git-deploy-prod

