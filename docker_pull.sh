#!/bin/bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "Pull containers for stack $STACK_NAME"


docker-compose pull
