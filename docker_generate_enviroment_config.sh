#!/usr/bin/env bash

source docker/bash_functions.sh


STACK_NAME="lumen_docker_base"
SYSROOT=$(pwd)



HOSTNAME=$1
if [[ $HOSTNAME != *[!\ ]* ]]; then
    HOSTNAME=localhost;
fi

USE_HTTPS=true

echo ""
printHeaderBlock "Generate valid configuration for enviroment"
echo ""



printHeaderBlock "Setup environment for stack .stack_env"

mkdir -p environment/
cd $SYSROOT/environment/
if [ ! -e .stack_env ]; then
    printNoticeBlock ".stack_env not found, generate new for $HOSTNAME"

    cp $SYSROOT/docker/.stack_env .stack_env
    sed -i "" "s/STACK_NAME=/STACK_NAME=$STACK_NAME/g" .stack_env
    sed -i "" "s/STACK_HOST=/STACK_HOST=$HOSTNAME/g" .stack_env
    if [ $USE_HTTPS ]; then
        sed -i "" "s/STACK_PROTOCOL=/STACK_PROTOCOL=https:\/\//" .stack_env
    else 
        sed -i "" "s/STACK_PROTOCOL=/STACK_PROTOCOL=http:\/\//" .stack_env
    fi;    
    
else
    printInfoBlock "Block .stack_env exists"
fi;
echo ""
importStackEnv



printHeaderBlock "Setup default environment for app app/.env"

mkdir -p $SYSROOT/environment/app
cd $SYSROOT/environment/app
if [ ! -e .env ]; then
    
    printNoticeBlock ".env not found, generate new for $HOSTNAME"
    
    sed "s/HOSTNAME/$HOSTNAME/" $SYSROOT/docker/app/.env_sample > .env
    KEY=$(docker run --rm -it sofianinho/pwgen-alpine 32 1)
    sed -i "" "s/APP_KEY=/APP_KEY=$KEY/g" .env
else
    printInfoBlock "Block .env exists"
fi;
echo ""


printHeaderBlock "Setup CA root certificate web/certs/root-ca"

mkdir -p $SYSROOT/environment/web/certs/root-ca
cd $SYSROOT/environment/web/certs
if [ ! -e site.key ]; then
    cd $SYSROOT/environment/web/certs/root-ca
    if [ ! -e root-ca.key ]; then
        
        printNoticeBlock "CA root certificate not found, generate new"

        openssl genrsa -out "root-ca.key" 4096

        openssl req \
                  -new -key "root-ca.key" \
                  -out "root-ca.csr" -sha256 \
                  -subj '/C=FI/ST=FI/L=Lahti/O=InfoMonitorManager/CN=SSL Devel Root CA'


        echo "[root_ca]
        basicConstraints = critical,CA:TRUE,pathlen:1
        keyUsage = critical, nonRepudiation, cRLSign, keyCertSign
        subjectKeyIdentifier=hash" > root-ca.cnf


        openssl x509 -req -days 3650 -in "root-ca.csr" \
                       -signkey "root-ca.key" -sha256 -out "root-ca.crt" \
                       -extfile "root-ca.cnf" -extensions \
                       root_ca
    else
        echo ""
        printInfoBlock "Block CA root sertificate exists"
    fi;
else
    printInfoBlock "Block site sertificate exist, skip CA root certificate creation"
fi;

echo ""



printHeaderBlock "Setup site certificate web/certs/"

cd $SYSROOT/environment/web/certs
if [ ! -e site.key ]; then
    
    printNoticeBlock "Site certificate not found, generate new"

    echo "[server]
    authorityKeyIdentifier=keyid,issuer
    basicConstraints = critical,CA:FALSE
    extendedKeyUsage=serverAuth
    keyUsage = critical, digitalSignature, keyEncipherment
    subjectAltName = DNS:$HOSTNAME, IP:127.0.0.1
    subjectKeyIdentifier=hash" > site.cnf

    openssl genrsa -out "site.key" 4096

    openssl req -new -key "site.key" -out "site.csr" -sha256 \
              -subj "/C=FI/ST=FI/L=Lahti/O=InfoMonitorManager/CN=$HOSTNAME"

    openssl x509 -req -days 750 -in "site.csr" -sha256 \
        -CA "root-ca/root-ca.crt" -CAkey "root-ca/root-ca.key" -CAcreateserial \
        -out "site.crt" -extfile "site.cnf" -extensions server

    rm site.cnf site.csr
else
    printInfoBlock "Block Site certificate exists"
fi;
echo ""



printHeaderBlock "Setup web server configuration web/webserver.conf"

mkdir -p $SYSROOT/environment/web
cd $SYSROOT/environment/web/
if [ ! -e webserver.conf ]; then
    
    printNoticeBlock "Web server configuration not found, generate new for $HOSTNAME"

    sed "s/HOSTNAME/$HOSTNAME/" $SYSROOT/docker/web/webserver_for_HTTPS.conf > webserver_for_HTTP.conf
    sed "s/HOSTNAME/$HOSTNAME/" $SYSROOT/docker/web/webserver_for_HTTPS.conf > webserver_for_HTTPS.conf
    ln -s webserver_for_HTTPS.conf webserver.conf
else
    printInfoBlock "Block Web server configuration exists"
fi;
echo ""

printNoticeBlock "Environment setup completed"
printImportantBlock "To regenerate remove all content from environment folder and run script again"

echo ""