#!/usr/bin/env bash

source docker/bash_functions.sh
importStackEnv

printHeaderBlock "Removing stack $STACK_NAME"


docker stack rm lumen_docker_base