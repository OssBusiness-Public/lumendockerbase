FROM nginx:alpine

LABEL   "fi.oss-solutions.project"="lumen_docker_base" \
        "fi.oss-solutions.service"="web" \
        "fi.oss-solutions.autoregister.hostname"="infomonitormanager.local"

RUN rm -f /etc/nginx/conf.d/default.conf

ADD git-deploy-prod /var/www