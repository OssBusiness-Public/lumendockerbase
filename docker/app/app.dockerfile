FROM php:7.2-fpm-alpine3.7

LABEL   "fi.oss-solutions.project"="lumen_docker_base" \
        "fi.oss-solutions.service"="app"

RUN apk update && apk add libmcrypt-dev \
        libxml2-dev \
        icu-dev \
        freetype-dev \
		libjpeg-turbo-dev \
		libpng-dev

RUN docker-php-ext-install exif zip

RUN adduser -S appuser && \
    (echo "* * * * * /usr/local/bin/php /var/www/artisan schedule:run >> /dev/null 2>&1") > /var/spool/cron/appuser

ADD git-deploy-prod/app/ /var/www
ADD app/vendor /var/www/vendor
RUN chown -R appuser:www-data /var/www/

RUN rm -fr /tmp/*

USER appuser:www-data


