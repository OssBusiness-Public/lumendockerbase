#!/bin/bash

function importStackEnv() {
	if [ -f .stack_env ]; then
	   export $(grep -v '^#' .stack_env | xargs)
	else
	   export $(grep -v '^#' environment/.stack_env | xargs)
	fi
	
}

function printHeaderBlock() {
	STRING_LEN=${#1}
	PAD_LEN=4
	HASH_LENGTH=$(($STRING_LEN + $PAD_LEN))

	PHASH="printf '%0.1s' "\\#"{1..$HASH_LENGTH}"
	echo
	eval $PHASH
	echo 
	echo "# $1 #"
	eval $PHASH
	echo
	echo	
}


function printInfoBlock() {
	echo ""
	echo "* $1 *"
	echo ""
}

function printNoticeBlock() {
	echo ""
	echo "*** $1 ***"
	echo ""
}

function printImportantBlock() {
	echo ""
	echo "!!! $1 !!!"
	echo ""
}